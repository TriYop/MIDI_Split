# MIDI_Split

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/0276a6f9772c4d859b63e1c1c63bafbe)](https://www.codacy.com/app/TriYop/MIDI_Split?utm_source=github.com&utm_medium=referral&utm_content=TriYop/MIDI_Split&utm_campaign=badger)
[![Known Vulnerabilities](https://snyk.io/test/github/triyop/midi_split/badge.svg)](https://snyk.io/test/github/triyop/midi_split)

Splits MIDI files into multiple MIDI files containing a single instrument in order to render individualy in a DAW.

each MIDI channel except channel 10 (reserved for drums) is split based on MIDI program to enable per-instrument mix instead of per MIDI channel.

Drum channel is split based on instrument : 
- by default, each note is considered a single instrument
- in some obvious cases, instruments may be grouped in a single track to allow same instrument with its variants to be mixed as a single channel in a DAW. 
  - Hi Hat
  - Conga
  - Bongo
  - Agogo
  - Triangle
  - Cuica
  - Guiro
  - Snare
  - Woodblock
  - Timbale
  
  



