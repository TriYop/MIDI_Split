import getopt

from mido import MidiFile, Message, MetaMessage, MidiTrack

from miditools import MIDISplit
from miditools.MIDIConstants import  *
import os

def test_write_empty_midi_file():
    MIDISplit.write_midi_file("test.mid", MidiFile())
    assert True


def test_write_midi_file():
    output = MidiFile()
    output.add_track(name="test_track")
    output.ticks_per_beat = 724
    output.type = 1
    MIDISplit.write_midi_file("test.mid", output)

def test_check_file():
    f,b = MIDISplit.check_file("")
    assert not b
    assert f is None

    with open("test_check_file",'w') as f:
        f.write("test")
    f,b = MIDISplit.check_file("test_check_file")
    assert b
    os.remove("test_check_file")

def test_check_dir():
    f, b = MIDISplit.check_dir("")
    assert not b
    assert f is None

    f,b = MIDISplit.check_dir("test_check_dir")
    assert b
    os.removedirs("test_check_dir")



def test_parse_empty_options():
    argv = []
    input_file, output_dir, gd = MIDISplit.parse_options(argv)

    assert input_file is None
    assert output_dir is None

def test_parse_verbose_options():
    argv = ["-v"]
    input_file, output_dir, gd = MIDISplit.parse_options(argv)

    assert input_file is None
    assert output_dir is None

def test_parse_unknown_options():
    argv = ["-itest.mid","-otest","-p","--kiwi"]
    input_file, output_dir, gd = MIDISplit.parse_options(argv)

    assert input_file is None
    assert output_dir is None

def test_parse_help_options():
    argv = ["-h"]
    input_file, output_dir, gd = MIDISplit.parse_options(argv)

    assert input_file is None
    assert output_dir is None


def test_parse_valid_options():
    argv = ["-itest.mid","--output=test","-g"]

    input_file, output_dir, gd = MIDISplit.parse_options(argv)

    os.remove("test.mid")
    os.removedirs("test")

    assert input_file == "test.mid"
    assert output_dir == "test"
    assert gd == False

def test_split_drums():
    messages = [{'message':Message('note_on', note=MIDI_DRUMS_NAMES['Kick-in'], velocity=3, time=0),'abs_time': 0},
    {'message':Message('note_on', note=MIDI_DRUMS_NAMES['Snare-top'], velocity=3, time=0),'abs_time': 0},
    {'message':Message('note_off', note=MIDI_DRUMS_NAMES['Snare-bottom'], velocity=3, time=0),'abs_time': 0},
    {'message':Message('note_on', note=MIDI_DRUMS_NAMES['Snare-ring'], velocity=3, time=0),'abs_time': 0}]

    res = MIDISplit.split_drums(messages, False)
    assert "Snare-bottom" not in res, 'note_off messages should not create track'
    assert "Snare" not in res, 'tracks should not be grouped'
    assert 3==len(res)
    res = MIDISplit.split_drums(messages, True)
    assert 2 == len(res)
    assert "Snare" in res, 'tracks should not be grouped'
    assert "Snare-top" not in res, 'tracks should not be grouped'

def test_split_instruments():
    messages = [{'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Kick-in'], velocity=3, time=0),
                 'abs_time': 0},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program= MIDI_PROGRAMS_NAMES['Violin'], time=1),
                 'abs_time': 1},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Oboe'], time=2),
                 'abs_time': 2},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Bassoon'], time=3),
                 'abs_time': 3},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Oboe'], time=4),
                 'abs_time': 4},
                {'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-top'], velocity=3, time=0),
                 'abs_time': 0},
                {'message': Message(MIDI_MESSAGE_NOTE_OFF, note=MIDI_DRUMS_NAMES['Snare-bottom'], velocity=3,
                                    time=0), 'abs_time': 0},
                {'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-ring'], velocity=3, time=0),
                 'abs_time': 0}]

    res = MIDISplit.split_instruments(messages)
    assert 4 == len(res)
    assert MIDI_PROGRAMS_NAMES['Violin'] in res
    assert 0 in res

def test_list_to_track():
    messages = [{'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Kick-in'], velocity=3, time=0),
                 'abs_time': 0},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Violin'], time=1),
                 'abs_time': 1},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Oboe'], time=2),
                 'abs_time': 2},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Bassoon'], time=3),
                 'abs_time': 3},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Oboe'], time=4),
                 'abs_time': 4},
                {'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-top'], velocity=3, time=0),
                 'abs_time': 0},
                {'message': Message(MIDI_MESSAGE_NOTE_OFF, note=MIDI_DRUMS_NAMES['Snare-bottom'], velocity=3,
                                    time=0), 'abs_time': 0},
                {'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-ring'], velocity=3, time=0),
                 'abs_time': 0}]
    track = MIDISplit.list_to_track(messages)
    assert len(messages) == len(track)
    assert track[0].time == 0
    assert track[1].time == 1
    assert track[2].time == 1

def test_append_metas():
    messages = [{'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Kick-in'], velocity=3, time=0), 'abs_time': 0},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Violin'], time=1),    'abs_time': 1},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Oboe'], time=1),     'abs_time': 2},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Bassoon'], time=1),  'abs_time': 3},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Oboe'], time=1),     'abs_time': 4},
                {'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-top'], velocity=3, time=4), 'abs_time': 8},
                {'message': Message(MIDI_MESSAGE_NOTE_OFF, note=MIDI_DRUMS_NAMES['Snare-bottom'], velocity=3, time=10), 'abs_time': 18},
                {'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-ring'], velocity=3, time=2), 'abs_time': 20}]

    metas = [{'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Kick-in'], velocity=3, time=1), 'abs_time': 1},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Violin'], time=4),'abs_time': 5},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Oboe'], time=2), 'abs_time': 7},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Bassoon'], time=3), 'abs_time': 10},
                {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program=MIDI_PROGRAMS_NAMES['Oboe'], time=4), 'abs_time': 14},
                {'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-top'], velocity=3, time=0), 'abs_time': 14},
                {'message': Message(MIDI_MESSAGE_NOTE_OFF, note=MIDI_DRUMS_NAMES['Snare-bottom'], velocity=3, time=3), 'abs_time': 17},
                {'message': Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-ring'], velocity=3, time=1), 'abs_time': 18}]
    track= MIDISplit.append_metas(messages, metas)

    print(messages)
    print(metas)
    print(track)

    assert len(track) == (len(messages)+len(metas))


def test_filter_events():
    track = MidiTrack()
    track.append(MetaMessage('key_signature',  key='C#', time=0))
    channels, metas = MIDISplit.filter_events(track)
    assert 1 == len(metas)
    assert 0 == len(channels)

    track = MidiTrack()
    track.append(Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-ring'], velocity=3, time=1))
    channels, metas = MIDISplit.filter_events(track)
    assert 0 == len(metas)
    assert 1 == len(channels)
    assert 1 == len(channels['c1'])

    track = MidiTrack()
    track.append(MetaMessage('key_signature', key='C#', time=0))
    track.append(MetaMessage('text', text='Ceci est un texte', time=0))
    track.append(Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-ring'], velocity=3, time=1))
    track.append(Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-top'], velocity=3, time=2))
    track.append(Message(MIDI_MESSAGE_NOTE_ON, note=MIDI_DRUMS_NAMES['Snare-top'], velocity=3, time=2, channel=3))

    channels, metas = MIDISplit.filter_events(track)
    assert 1 == len(metas)
    assert 2 == len(channels)
    assert 2 == len(channels['c1'])
    assert 1 == len(channels['c4'])





def test_do_split():
    pass