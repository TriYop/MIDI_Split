from miditools import MIDIConstants


def test_lists_sizes():
    assert len(MIDIConstants.MIDI_VALID_NOTES) == 128
    assert len(MIDIConstants.MIDI_NOTES) == len(MIDIConstants.MIDI_NOTES_NAMES)
    assert len(MIDIConstants.MIDI_PROGRAMS) == 128
    assert len(MIDIConstants.MIDI_DRUMS_NAMES) == len(MIDIConstants.MIDI_DRUMS)
    assert len(MIDIConstants.MIDI_DRUMS_GROUPS_MAP) > len(MIDIConstants.MIDI_DRUMS_GROUPS)

def test_drum_groups_content():
    drums = []
    for k,v in MIDIConstants.MIDI_DRUMS_GROUPS.items():
        for drum in v:
            assert drum in MIDIConstants.MIDI_DRUMS_NAMES, "Unknown drum %s in group %s" %(drum, k)
            assert drum not in drums, "Drum %s is declared to belong to more than one group" % drum
            drums.append(drum)

