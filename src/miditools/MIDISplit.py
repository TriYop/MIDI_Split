#!/usr/bin/env python3
"""

"""
#
# Separate MIDI channels from a MIDI FILE
#

import getopt

import logging
import os
import sys

from mido import MidiFile, MetaMessage, MidiTrack, Message

from miditools import MIDICheck
from miditools.MIDIConstants import *

CHANNEL_MESSAGES = ['note_off', 'note_on', 'polytouch', 'control_change', 'program_change', 'pitchwheel', 'aftertouch']
logging.basicConfig(level=logging.INFO, format=MIDI_LOG_FORMATTER)
logger = logging.getLogger("miditools.MIDISplit")


def write_midi_file(filename, pattern):
    """
    Outputs a MIDI pattern as a MIDI file
    :param filename:
    :param pattern:
    :return:
    """
    if len(pattern.tracks) > 0 and len(pattern.tracks[0]) > 0:
        # TODO: reinclude all META MESSAGES BEFORE SAVING
        pattern.save(filename)
    else:
        logger.info("Not generating output for empty pattern.")


def filter_events(pattern):
    """
    Splits events from the input MIDI file to separate MIDI channels and a META channel
    :param pattern:
    :return: an array of channels and a meta channel (array of META messages)
    """
    channels = {}
    metas = []
    current_time = 0.
    for message in pattern:
        current_time += int(message.time * 960)

        if isinstance(message, MetaMessage):
            if message.type != 'text':
                metas.append({'message': message, 'abs_time': current_time})

        elif message.type in CHANNEL_MESSAGES:
            # We do +1 because MIDI channels usually start numbering at 1 and channel 10 is usually drums one.
            chan = 'c' + str(message.channel + 1)
            if chan not in channels:
                channels[chan] = []
            channels[chan].append({'message': message, 'abs_time': current_time})

        else:
            logger.error("Caught unexpected MIDI message: %s" % repr(message))

    return channels, metas


def list_to_track(evt_list):
    """
    Converts a list of events to a MIDI track with relative times
    :param evt_list:
    :return:
    """
    track = MidiTrack()
    current_time = 0

    for event in evt_list:
        msg = event['message']
        atm = event['abs_time']
        rel_time = int(atm - current_time)
        msg.time = rel_time
        track.append(msg)
        current_time = atm

    return track


def split_drums(channel, group_by_instrument=True):
    """
    Splits a list of drum events to a set of drum events with a single instrument per track.

    TODO: group related instruments in a single track (open/pedal/closed Hi Hat, mute/open triangle, mute/open quica, )

    :param channel:
    :return:
    """
    tracks = {}
    for event in channel:
        msg = event['message']
        atm = event['abs_time']
        if msg.type == 'note_on':
            drumname = MIDI_DRUMS[msg.note]
            trackname = drumname
            if group_by_instrument and drumname in MIDI_DRUMS_GROUPS_MAP:
                trackname =  MIDI_DRUMS_GROUPS_MAP[drumname]

            if not trackname in tracks:
                tracks[trackname] = []
            tracks[trackname].append({'message': msg, 'abs_time': atm})

    return tracks

def split_instruments(channel):
    """
    Splits a single MIDI channel into as many single instrument channels.

    :param channel:
    :return:
    """
    tracks = {}
    track = []

    if (channel[0]['message'].type != MIDI_MESSAGE_PGM_CHANGE):
        channel.insert(0, {'message': Message(MIDI_MESSAGE_PGM_CHANGE, program= DEFAULT_PROGRAM, time=0),
                 'abs_time': 0})

    for event in channel:
        msg = event['message']
        atm = event['abs_time']

        if msg.type == MIDI_MESSAGE_PGM_CHANGE:
            pgm = msg.program
            if pgm not in tracks:
                track = []
                tracks[pgm] = track
                track.append({'message': msg, 'abs_time': atm})
            else:
                track = tracks[pgm]
        else:
            track.append({'message': msg, 'abs_time': atm})

    return tracks


def usage(msg=""):
    """
    Displays expected options to be passed when calling this program
    :param msg:
    :return:
    """
    if msg != "":
        print("Launch returned error: %s" % msg)

    print("\nOptions: ")
    print("  -i|--input <INPUT FILE PATH>   : Defines input MIDI file")
    print("  -o|--output <OUTPUT PATH>      : Defines ouptut directory")
    print("  -v|--verbose                   : sets verbose output")
    print("  -g|--no-group                  : disables drums grouping")
    print("  -h|--help                      : displays this message")


def append_metas(track, metas):
    """
    Appends META information to any list of events.
    :param track:
    :param metas:
    :return:
    """
    pattern = []
    chan = -1
    midx = 0
    metan = len(metas)

    for evt in track:
        msg = evt['message']
        evt_time = evt['abs_time']

        if chan == -1:
            chan = msg.channel

        while midx < metan and metas[midx]['abs_time'] <= evt_time :
            meta = metas[midx]
            logger.debug('%8d - Adding META %s' % (meta['abs_time'], repr(meta['message'])))
            pattern.append(meta)
            midx += 1

        pattern.append(evt)
    return pattern


def do_split(input_file, output_dir, group_drums=True):
    """
    Parses MIDI file and splits tracks
    :param input_file:
    :param output_dir:
    :return:
    """
    in_pattern = MidiFile(input_file)
    base_name = os.path.basename(input_file).rsplit('.', 1)[0]
    channels, metas = filter_events(in_pattern)
    logger.debug(repr(metas))

    for chan in channels:
        if chan=="c10":

            drums= split_drums(channels[chan], group_drums)
            if drums is not None:
                for instru in drums:
                    fname = os.path.join(output_dir, "%s_%s-d%s.mid" % (base_name, chan, instru))
                    logger.debug("Output file: %s" % fname)
                    out_pattern = MidiFile(ticks_per_beat=in_pattern.ticks_per_beat, charset=in_pattern.charset,
                                           type=in_pattern.type)
                    out_pattern.tracks.append(list_to_track(append_metas(drums[instru], metas)))
                    write_midi_file(fname, out_pattern)

        else:
            instruments = split_instruments(channels[chan])
            for instrument in instruments:
                # Checks if instrument is valid
                MIDICheck.check_channel(instrument, instruments[instrument])
                instru = MIDI_PROGRAMS[instrument-1];
                logger.warning("Exporting instrument %s on channel %s" % (instru, chan))
                fname = os.path.join(output_dir, "%s_%s-p%s.mid" % (base_name, chan, instru))
                logger.debug("Output file: %s" % fname)
                out_pattern = MidiFile(ticks_per_beat=in_pattern.ticks_per_beat, charset=in_pattern.charset,
                                       type=in_pattern.type)
                out_pattern.tracks.append(list_to_track(append_metas(instruments[instrument], metas)))
                write_midi_file(fname, out_pattern)

def check_file(filename):
    """
    Checks if filename matches an existing file
    :param filename:
    :return:
    """
    if os.path.isfile(filename):
        return filename, True
    else:
        logger.error("File '%s' not found." % filename)
        return None, False

def check_dir(dirname):
    """
    CHecks if dirname matches an existing dir or tries to create it if not
    :param dirname:
    :return:
    """
    if not os.path.exists(dirname):
        logger.debug("Creating non existing output directory.")
        try:
            os.mkdir(dirname)
        except FileNotFoundError:
            logger.error("Filename may be invalid.")

    if os.path.isdir(dirname):
        return dirname, True
    else:
        logger.error("Directory '%s' does not exist and was not created." % dirname)
        return None, False


def parse_options(argv):
    """
    Interprets command line parameters and args
    :param argv:
    :return:
    """
    try:
        optlist, args = getopt.getopt(argv, 'hgvi:o:', ['help', 'verbose', 'no-group', 'input=', 'output='])
    except getopt.GetoptError as error:
        logger.error(error)
        usage()
        return None, None, False

    input_file = ""
    output_dir = ""
    input_set = False
    output_set = False
    verbose = False
    group_drums = True

    for o, a in optlist:

        if o in ['-h', '--help']:
            usage()
            return None, None, False
        elif o in ["-g", "--no-group"]:
            group_drums = False
        elif o in ["-v", "--verbose"]:
            verbose = True
        elif o in ['-i', '--input']:
            input_file, input_set = check_file(a)
        elif o in ['-o', '--output']:
            output_dir, output_set = check_dir(a)
        else:
            logger.error("Invalid option")
            return None, None, False

    if verbose:
        logging.basicConfig(filename='MIDI_split.log', level=logging.DEBUG)
        logger.setLevel(logging.DEBUG)
        logger.info("Logging set to verbose level.")

    else:
        logging.basicConfig(filename='MIDI_split.log', level=logging.WARNING)
        logger.warning("Logging set to quiet level.")

    if not (input_set and output_set):
        usage("Missing input or output files")
        return None, None, False

    return input_file, output_dir, group_drums


if __name__ == '__main__':
    try:
        input_file, output_dir, group_drums = parse_options(sys.argv[1:])
        if input_file is not None and output_dir is not None:
            do_split(input_file, output_dir, group_drums)
        else:
            usage()
            exit(2)
    except AssertionError as err:
        usage(msg=str(err))
        exit(3)
