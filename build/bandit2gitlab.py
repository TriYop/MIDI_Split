"""Codeclimate compatible JSON reporter"""
import hashlib
import json
import os
import sys


class CodeClimateConverter(object):
    """converts a Bandit JSON report to a gitlab compliant code quality report"""

    exported = False

    severity_map = {
        'LOW': 'info',
        'MEDIUM': 'normal',
        'HIGH': 'critical',
    }

    def __init__(self, filename):
        """Creates a new converter"""
        if not os.path.isfile(filename):
            print("ERROR: input file '%s' does not exist. Won't do anything." % filename)
            return
        self.filename = filename
        self.messages = []
        self.load()

    def load(self):
        """
        Loads bandit report file
        :return:
        """
        with open(self.filename, 'r') as inputfile:
            filelines = inputfile.read()

        bandit_report= json.loads(filelines)
        for issue in bandit_report['results']:
            self.append_message(issue)

    def append_message(self, message):
        """Manage message of different type and in the context of path."""

        sha = hashlib.sha256()
        sha.update(message.get('code').encode())
        sha.update(message.get('filename').encode())
        sha.update(str(message.get('line_number')).encode())
        fingerprint = sha.hexdigest()

        msg = {
            'check_name': message.get('test_id'),
            'description': message.get('issue_text'),
            'categories': "security",
            'fingerprint': fingerprint,
            'location': {
                'path': message.get('filename'),
                'lines': {
                    'begin': message.get('line_number'),
                },
            },
            'remediation_points': 150000,
            'severity': self.severity_map.get(message.get('issue_severity')),
        }

        # Apply overrides
        self.messages.append(msg)


    def export(self, filename:str):
        """Exports messages to a gitlab compliant format"""
        if os.path.isfile(filename):
            print("ERROR: output file %s already exist. Won't replace." % filename)
            return

        if self.exported:
            print("ERROR: Report already exported. Won't replace nor append.")
            return

        with open(filename, 'w') as ouputfile:
            self.exported = True
            ouputfile.write(json.dumps(self.messages, indent=4))




if __name__ == '__main__':
    if len(sys.argv)>2:
        cc = CodeClimateConverter(sys.argv[1])
        cc.export(sys.argv[2])
    else:
        print("ERROR: missing arguments.")
        print("  %s input_json output_json" % sys.argv[0])
