""" Gitlab Codeclimate compatible JSON reporter"""
import hashlib
import json
import os

from message_overrides import OVERRIDES
from pylint.interfaces import IReporter
from pylint.reporters import BaseReporter


class CodeClimateReporter(BaseReporter):
    """Report messages and layouts in JSON."""

    __implements__ = IReporter
    exported = False
    name = 'codeclimate'
    extension = 'json'
    severity_map = {
        'info': 'info',
        'convention': 'info',
        'refactor': 'info',
        'warning': 'normal',
        'error': 'critical',
        'fatal': 'critical',
    }

    def __init__(self, *args, **kwargs):
        super(CodeClimateReporter, self).__init__(*args, **kwargs)
        self.config = None
        self.messages = []
        full_path = os.path.abspath('config.json')
        if os.path.isfile(full_path):
            with open('config.json') as config_file:
                try:
                    self.config = json.loads(config_file.read())
                except json.JSONDecodeError as ex:
                    print(ex)

    def handle_message(self, message):
        """Manage message of different type and in the context of path."""

        if self.config:

            if './' not in self.config['include_paths']:
                for include_path in self.config['include_paths']:
                    if message.path.startswith(include_path):
                        break
                else:
                    return

            for exclude_path in self.config['exclude_paths']:
                if message.path.startswith(exclude_path):
                    return

        m = hashlib.sha256()
        m.update(message.symbol.encode())
        m.update(message.path.encode())
        m.update(str(message.line).encode())
        fingerprint = m.hexdigest()

        msg = {
            'check_name': message.symbol,
            'description': message.msg.replace('\'', '`').splitlines()[0],
            'content': {
                'body': message.msg,
            },
            'fingerprint': fingerprint,
            'location': {
                'path': message.path,
                'lines': {
                    'begin': message.line,
                },
            },
            'remediation_points': 150000,
            'severity': self.severity_map[message.category],
        }
        if '\n' in message.msg:
            msg['content']['body'] += "\nDetails:\n" + message.msg

        # Apply overrides
        msg.update(OVERRIDES.get(message.symbol, {}))
        self.messages.append(msg)

    def _display(self, layout):
        """Reports are not supported."""
        if not self.exported:
            self.writeln(json.dumps(self.messages, indent=4))
        self.exported = True
